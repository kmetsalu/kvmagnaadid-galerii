/**
 * Created with JetBrains PhpStorm.
 * User: Kristjan
 * Date: 23.03.13
 * Time: 19:32
 * To change this template use File | Settings | File Templates.
 */
jQuery(document)
    .ready(function ($) {
        $("div.logoparade").smoothDivScroll({
            hotSpotScrolling: false,
			touchScrolling: true,
			manualContinuousScrolling: true,
			mousewheelScrolling: false
        });
    });